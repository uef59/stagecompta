
<div>
<table class="striped">
 <thead>
   <tr>
     <th>Id du client</th>
     <th>Société</th>
     <th>Nom</th>
     <th>Prenom</th>
     <th>Tel</th>
     <th>Adresse</th>
   </tr>
 </thead>
 <tbody>

   <?php foreach($clients as $r){?>

   <tr>
      <td> <?php echo $r['idClient'];?> </td>
      <td> <a href="../controller/profilclient.php?idClient=<?php echo $r['idClient'];?>"> <?php echo $r['nomSoc'];?></a> </td>
      <td> <?php echo $r['nom'];?> </td>
      <td> <?php echo $r['prenom'];?> </td>
      <td> <?php echo $r['tel'];?> </td>
      <td> <?php echo $r['adresse'] . ' ' . $r['codePostal'] . ' ' . $r['ville'];?> </td>

   </tr>
  <?php } ?>

 </tbody>
</table>

</div>


<br>
<div class="row">
<div class="center">
  <a class="waves-effect waves-light btn indigo darken-1" href="../controller/boutonajoutclient.php">Ajouter un client</a>
</div>
</div

</body>
