
	<div>
		<form action="../controller/ajoutclient.php" method="post">

			<div class="row">
				<div class="input-field col s5">
					<input type="text" name="nom"/>
					<label class="active" for="nom">Nom</label>
				</div>

				<div class="input-field col s5">
					<input type="text" name="prenom"/>
					<label class="active" for="prenom">Prenom</label>
				</div>
			</div>


			<div class="row">
				<div class="input-field col s5">
					<input type="text" name="nomSoc"/>
					<label class="active" for="nomSoc">Societe</label>
				</div>

				<div class="input-field col s5">
					<input type="text" name="tel"/>
					<label class="active" for="tel">Telephone</label>
				</div>
			</div>


			<div class="row">
				<div class="input-field col s5">
					<input type="text" name="adresse"/>
					<label class="active" for="adresse">Adresse</label>
				</div>

				<div class="input-field col s5">
					<input type="text" name="ville"/>
					<label class="active" for="ville">Ville</label>
				</div>
			</div>


			<div class="row">
				<div class="input-field col s5">
					<input type="text" name="codePostal"/>
					<label class="active" for="codePostal">Code Postal</label>
				</div>
			</div>


			<div class="row">
				<div class="input-field col s5">
					<input class="btn waves-effect waves-light indigo darken-1" type="submit" value="Inscrire"/>
				</div>
			</div>

		</form>
	</div>
</body>
