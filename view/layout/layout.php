<!-- LAYOUT SITE COMPLET  -->
<!DOCTYPE html>
<html lang="en">


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
<title>Projet compta</title>


<!-- CSS  -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="../css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
<link href="../css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
<link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
<link href="../css/modif.css" type="text/css" rel="stylesheet" media="screen,projection"/>


<nav class="indigo darken-1" role="navigation">


  <div class="nav-wrapper container"><a id="logo-container" href="../controller/index.php" class="brand-logo">Projet compta</a>

    <ul class="right hide-on-med-and-down">

      <?php
        echo '<li><a href="../controller/index.php">Accueil</a></li>';
        echo '<li><a href="../controller/client.php">Clients</a></li>'
       ?>

    </ul>

  </div>

</nav>
<meta name = "viewport" content = "width = device-width, initial-scale = 1">
    <link rel = "stylesheet"
       href = "https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel = "stylesheet"
       href = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
    <script type = "text/javascript"
       src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js">
    </script>

    <script>
       $(document).ready(function() {
          $('select').material_select();
       });
    </script>


</head>

<body>
