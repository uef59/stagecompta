<?php

class ecriturebancaire{
  private $idEcriture;
  private $dateEcri;
  private $encaissementTTC;
  private $decaissementTTc;
  private $commentaire;
  private $TVA;
  private $lib;
  private $idClient;

  function __construct(){
    $this->idEcriture= $idEcriture;
    $this->dateEcri= $dateEcri;
    $this->encaissementTTC= $encaissementTTC;
    $this->decaissementTTC= $decaissementTTC;
    $this->commentaire= $commentaire;
    $this->TVA= $TVA;
    $this->lib= $lib;
    $this->idClient= $idClient;
  }

  public function getIdEcriture(){
    return $this->idEcriture;
  }

  public function getDateEcri(){
    return $this->dateEcri;
  }
  public function setDateEcri($dateEcri){
    $this->dateEcri= $dateEcri;
  }

  public function getEncaissementTTC(){
    return $this->encaissementTTC;
  }
  public function setEncaissementTTC(){
    $this->encaissementTTC= $encaissementTTC;
  }


  public function getDecaissementTTC(){
    return $this->decaissementTTC;
  }
  public function setDecaissementTTC(){
    $this->decaissementTTC= $decaissementTTC;
  }


  public function getCommentaire(){
    return $this->commentaire;
  }
  public function setCommentaire(){
    $this->commentaire= $commentaire;
  }

  public function getTVA(){
    return $this->TVA;
  }
  public function setTVA(){
    $this->TVA= $TVA;
  }

  public function getLib(){
    return $this->lib;
  }
  public function setLib(){
    $this->lib= $lib;
  }
  public function getIdClient(){
    return $this->idClient;
  }

}


include('bdd.php');

  function listeecrit($idCLient){
    global $bdd;
    $req= $bdd->prepare('SELECT eb.idEcriture, eb.dateEcrit, eb.encaissementTTC, eb.decaissementTTC, eb.commentaire FROM client c, ecriturebancaire eb, listeecrit l WHERE idCLient = idClient AND c.idClient = l.idClient AND l.idEcriture = eb.idEcriture');
    $req-> execute(array(
        'idClient' => $idClient
      ));
    $result = $req->fetch();
    return $result;

  }

 ?>
