<?php

class livrepolice{
  private $idLivrePolice;
  private $numOrdre;
  private $dateAchat;
  private $dateVente;
  private $nomVendeur;
  private $genre;
  private $marque;
  private $immat;
  private $facture;
  private $annee;
  private $prixAchat;
  private $reglement;
  private $dateReglement;
  private $nomAcheteur;
  private $prixVente;
  private $modeReglement;
  private $marge;
  private $idTVA;

  function __construct(){
    $this->idLivrePolice= $idLivrePolice;
  }


  public function getIdLivrePolice(){
    return $this->idLivrePolice;
  }


  public function getnumOrdre(){
    return $this->numOrdre;
  }
  public function setNumOrdre($numOrdre){
    $this->numOrdre= $numOrdre;
  }


  public function getdateAchat(){
    return $this->dateAchat;
  }
  public function setDateAchat($dateAchat){
    $this->dateAchat= $dateAchat;
  }


  public function getDateVente(){
    return $this->dateVente;
  }
  public function setDateVente($dateVente){
    $this->dateVente= $dateVente;
  }


  public function getNomVendeur(){
    return $this->nomVendeur;
  }
  public function setNomVendeur($nomVendeur){
    $this->nomVendeur= $nomVendeur;
  }


  public function getGenre(){
    return $this->genre;
  }
  public function setGenre($genre){
    $this->genre= $genre;
  }


  public function getMarque(){
    return $this->marque;
  }
  public function setMarque($marque){
    $this->marque= $marque;
  }


  public function getImmat(){
    return $this->immat;
  }
  public function setImmat($immat){
    $this->immat= $immat;
  }


  public function getFacture(){
    return $this->facture;
  }
  public function setFacture($facture){
    $this->facture= $facture;
  }


  public function getAnnee(){
    return $this->annee;
  }
  public function setAnnee($annee){
    $this->annee= $annee;
  }


  public function getPrixAchat(){
    return $this->prixAchat;
  }
  public function setPrixAchat($prixAchat){
    $this->prixAchat= $prixAchat;
  }


  public function getReglement(){
    return $this->reglement;
  }
  public function setReglement($reglement){
    $this->reglement= $reglement;
  }


  public function getDateReglement(){
    return $this->dateReglement;
  }
  public function setDateReglement($dateReglement){
    $this->dateReglement= $dateReglement;
  }


  public function getNomAcheteur(){
    return $this->nomAcheteur;
  }
  public function setNomAcheteur($nomAcheteur){
    $this->nomAcheteur= $nomAcheteur;
  }


  public function getPrixVente(){
    return $this->prixVente;
  }
  public function setPrixVente($prixVente){
    $this->prixVente= $prixVente;
  }


  public function getModeReglement(){
    return $this->modeReglement;
  }
  public function setModeReglement($modeReglement){
    $this->modeReglement= $modeReglement;
  }


  public function getMarge(){
    return $this->marge;
  }
  public function setMarge($marge){
    $this->marge= $marge;
  }

  public function getIdTVA(){
    return $this->idTVA;
  }
  public function setIdTVA($idTVA){
    $this->idTVA= $idTVA;
  }

  public function getIdClient(){
    return $this->idClient;
  }
  public function setIdClient(){
    $this->idClient= $idClient;
  }

}



 ?>
