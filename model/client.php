<?php

class client
{
  private $idClient;
  private $nom;
  private $prenom;
  private $nomSoc;
  private $tel;
  private $adresse;
  private $ville;
  private $codePostal;

  function __construct($idClient, $nom, $prenom, $nomSoc, $tel, $adresse, $ville, $codePostal)
  {
    $this->idClient= $idClient;
    $this->nom= $nom;
    $this->prenom= $prenom;
    $this->nomSoc= $nomSoc;
    $this->tel= $tel;
    $this->adresse= $adresse;
    $this->ville= $ville;
    $this->codePostal= $codePostal;
  }

  public function getIdClient(){
    return $this->idClient;
  }

  public function getNom(){
    return $this->nom;
  }
  public function setNom($nom){
    $this->nom = $nom;
  }

  public function getPrenom(){
    return $this->prenom;
  }
  public function setPrenom($prenom){
    $this->prenom = $prenom;
  }

  public function getNomSoc(){
    return $this->nomSoc;
  }
  public function setNomSoc($nomSoc){
    $this->nomsSoc= $nomSoc;
  }

  public function getTel(){
    return $this->tel;
  }
  public function setTel($tel){
    $this->tel= $tel;
  }

  public function getAdresse(){
    return $this->adresse;
  }
  public function setAdresse($adresse){
    $this->adresse= $adresse;
  }

  public function getVille(){
    return $this->ville;
  }
  public function setVille($ville){
    $this->ville= $ville;
  }

  public function getCodePostal(){
    return $this->codePostal;
  }
  public function setCodePostal($codePostal){
    $this->codePostal= $codePostal;
  }

}

include('bdd.php');

function listeclient(){
  global $bdd;
  $req= $bdd->prepare('SELECT idClient, nomSoc, nom, prenom, tel, adresse, codePostal, ville FROM client');
  $req->execute();
  $result = $req->fetchAll();
  return $result;
}



function profilclient($idClient){
  global $bdd;
  $req = $bdd->prepare('SELECT * FROM client WHERE idClient = idCLient');
  $req->execute(array(
      'idClient' => $idClient
    ));
  $result = $req->fetch();
  return $result;
}

//requete a completer

function modifclient($idClient){
  global $bdd;
  $update = $bdd->prepare('UPDATE client(nomSoc, nom,  prenom, tel, adresse, codePostal, ville) SET(:nom, :prenom, :nomSoc, :tel, :adresse, :codePostal, :prix, :ville) WHERE id=$_GET["id"]');
  $update->execute(array(
        ':nomSoc' => ($_POST['nomSoc']),
        ':nom' => ($_POST['nom']),
        ':prenom' => ($_POST['prenom']),
        ':tel' => ($_POST['tel']),
        ':adresse' => ($_POST['adresse']),
        ':codePostal' => ($_POST['codePostal']),
        ':ville' => ($_POST['ville']),
    ));
  }
//requete a modifier

function supprimeclient($idClient){
  global $bdd;
  $req = $bdd ->prepare('DELETE idCLient, nomSoc, nom, prenom, tel, adresse, codePostal, ville FROM client WHERE idClient = :idClient');
  $req->bindParam('idClient', $idClient);
  $req->execute();
}

 ?>
